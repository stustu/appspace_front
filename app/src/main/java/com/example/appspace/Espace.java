package com.example.appspace;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Espace {
    private int id;
    private String nom;
    private int utilisateur_id;
    private List<Indicateur> indicateurs;

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public int getUtilisateur_id() {
        return utilisateur_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setUtilisateur_id(int utilisateur_id) {
        this.utilisateur_id = utilisateur_id;
    }

    public List<Indicateur> getIndicateurs() {
        return indicateurs;
    }

    public void setIndicateurs(List<Indicateur> indicateurs) {
        this.indicateurs = indicateurs;
    }
}
