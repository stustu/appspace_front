package com.example.appspace;

public class Login {
    private String pseudo;
    private String passward;

    public Login(String pseudo, String passward) {
        this.pseudo = pseudo;
        this.passward = passward;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getPassward() {
        return passward;
    }

    public void setPassward(String passward) {
        this.passward = passward;
    }

}
