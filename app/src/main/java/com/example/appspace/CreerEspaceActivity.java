package com.example.appspace;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CreerEspaceActivity extends AppCompatActivity {
    private EditText nomEspace;
    private Button creerEspace;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.espace_formuaire);

        Context ctx = this.getApplicationContext();

         creerEspace = findViewById(R.id.ButtonCreerEspace);
         creerEspace.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 nomEspace = findViewById(R.id.NomEspace);
                 String nom = nomEspace.getText().toString();

                 File.write("espaces.json","{\n" +
                         "\t\"id\": \"0\",\n" +
                         "\t\"nom\": \"" + nom + "\"\n" +
                         "  \t\"indicateurs\": []\n" +
                         "}" , ctx);
             }
         });
    }
}
