package com.example.appspace;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

public class MainActivity extends AppCompatActivity{

    private Button buttonLogin;
    private Button buttonSignup;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialisation des fichiers
        this.init();

        buttonLogin = findViewById(R.id.ButtonLogin);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent( MainActivity.this, LoginActivity.class);
                startActivity(menuIntent);
            }
        });

        buttonSignup = findViewById(R.id.ButtonSignup);
        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent( MainActivity.this, SignupActivity.class);
                startActivity(menuIntent);
            }
        });

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private Boolean init(){

        File.write("espaces.json","[{\n" +
                "\t\"id\": \"2\",\n" +
                "\t\"nom\": \"espace\",\n" +
                "\t\"indicateurs\": [{\n" +
                "\t\t\"id\": \"1\",\n" +
                "\t\t\"nom\": \"indicateur1\",\n" +
                "\t\t\"type\": \"STRING\",\n" +
                "\t\t\"valeurInt\": \"null\",\n" +
                "\t\t\"valeurString\": \"test\",\n" +
                "\t\t\"valeurBoolean\": \"null\"\n" +
                "\t}, {\n" +
                "\t\t\"id\": \"1\",\n" +
                "\t\t\"nom\": \"indicateur2\",\n" +
                "\t\t\"type\": \"INT\",\n" +
                "\t\t\"valeurInt\": \"1\",\n" +
                "\t\t\"valeurString\": \"null\",\n" +
                "\t\t\"valeurBoolean\": \"null\"\n" +
                "\t}]\n" +
                "}, {\n" +
                "\t\"id\": \"1\",\n" +
                "\t\"nom\": \"espace2\",\n" +
                "\t\"indicateurs\": [{\n" +
                "\t\t\"id\": \"1\",\n" +
                "\t\t\"nom\": \"indicateur1\",\n" +
                "\t\t\"type\": \"STRING\",\n" +
                "\t\t\"valeurInt\": \"null\",\n" +
                "\t\t\"valeurString\": \"test\",\n" +
                "\t\t\"valeurBoolean\": \"null\"\n" +
                "\t}, {\n" +
                "\t\t\"id\": \"1\",\n" +
                "\t\t\"nom\": \"indicateur2\",\n" +
                "\t\t\"type\": \"INT\",\n" +
                "\t\t\"valeurInt\": \"1\",\n" +
                "\t\t\"valeurString\": \"null\",\n" +
                "\t\t\"valeurBoolean\": \"null\"\n" +
                "\t}]\n" +
                "\n" +
                "}]", this.getApplicationContext());
        File.write("login.json", "[{\n" +
                "\t\"pseudo\": \"jean\",\n" +
                "\t\"password\": \"123\"\n" +
                "}]", this.getApplicationContext());

        return true;

    }

}
