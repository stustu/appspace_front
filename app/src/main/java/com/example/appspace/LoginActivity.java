package com.example.appspace;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.FileInputStream;

public class LoginActivity extends AppCompatActivity {

    private Button buttonSubmit;
    private EditText pseudoText;
    private EditText passwordText;
    private TextView textViewMDPFalse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginform);
        Context ctx = this.getApplicationContext();
        buttonSubmit = findViewById(R.id.ButtonSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pseudoText = findViewById(R.id.Pseudo);
                passwordText = findViewById(R.id.Password);
                textViewMDPFalse = findViewById(R.id.MDPFalse);

                String pseudo = pseudoText.getText().toString();
                String password = passwordText.getText().toString();

                try{

                    String sJsonLu = File.read("login.json", ctx);
                    JsonArray jsonArray = new JsonParser().parse(sJsonLu).getAsJsonArray();

                    for (JsonElement jsonElement : jsonArray) {
                        Login login = new Login(jsonElement.getAsJsonObject().get("pseudo").getAsString(), jsonElement.getAsJsonObject().get("password").getAsString());

                        if(login.getPassward().equals(password) && login.getPseudo().equals(pseudo)){
                            // Si le mot de passe et le pseudo correspondent à ceux stockés on change d'interface
                            Intent menuIntent = new Intent( LoginActivity.this, HomePage.class);
                            startActivity(menuIntent);
                        }
                        else{
                            textViewMDPFalse.setText("Mauvais mot de passe, réessayer svp");
                        }
                        jsonElement.getAsJsonObject().get("nom").getAsString();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }
}
