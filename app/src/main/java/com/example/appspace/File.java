package com.example.appspace;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

public class File {
    static public Boolean write(String filename, String fileContents, Context ctx){

        final GsonBuilder builder = new GsonBuilder();
        final Gson gson = builder.create();

        try {
            Log.d("FILENAME", filename);
            Log.d("FILECONTENTS", fileContents);
            FileOutputStream outputStream;
            outputStream = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(fileContents.getBytes());
        } catch (IOException ioe) {
            return false;
        }
        return true;
    }

    static public String read(String filename, Context ctx){

        try{
            String sJsonLu = "";
            FileInputStream inputStream = ctx.openFileInput(filename);
            int content;
            while((content = inputStream.read()) != -1){
                sJsonLu = sJsonLu +(char)content;
            }
            Log.d("CONTENT", sJsonLu);

            inputStream.close();

            return sJsonLu;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }
}
