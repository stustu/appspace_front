package com.example.appspace;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.lang.reflect.Array;

public class EspaceActivity extends AppCompatActivity {

    private Button buttonEspace;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.espace_visualisation);

        LinearLayout mainll = findViewById(R.id.LL);


        String sJsonLu = File.read("espaces.json", this.getApplicationContext());
        JsonArray jsonArray = new JsonParser().parse(sJsonLu).getAsJsonArray();

        for (JsonElement jsonElement : jsonArray) {

            String nomEspace = jsonElement.getAsJsonObject().get("nom").getAsString();
            LinearLayout linearLayout = new LinearLayout(getApplicationContext());
            TextView tvEspace = new TextView(getApplicationContext());
            linearLayout.setPadding(0, 15, 0, 15);
            tvEspace.setText(nomEspace);

            mainll.addView(tvEspace);

            JsonArray indicateurs = jsonElement.getAsJsonObject().get("indicateurs").getAsJsonArray();

            for(JsonElement indicateur : indicateurs){
                String nom = indicateur.getAsJsonObject().get("nom").getAsString();
                int id = indicateur.getAsJsonObject().get("id").getAsInt();

                TextView tv = new TextView(getApplicationContext());
                tv.setText(nom);
                tv.setId(id);
                linearLayout.addView(tv);
            }

        }


        buttonEspace = findViewById(R.id.CreerEspace);
        buttonEspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent(EspaceActivity.this, CreerEspaceActivity.class);
                startActivity(menuIntent);
            }

        });
    }
}
