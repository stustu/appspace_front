package com.example.appspace;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AppSpaceApi {

    @GET("?request=espaces&hash=4de02e477c04ca284900eff62314c0ea")
    Call<List<Espace>> getEspaces();
}
