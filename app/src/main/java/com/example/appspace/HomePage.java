package com.example.appspace;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomePage extends AppCompatActivity {
    private Button buttonEspace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);

        buttonEspace = findViewById(R.id.ButtonEspace);
        buttonEspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent menuIntent = new Intent( HomePage.this, EspaceActivity.class);
                startActivity(menuIntent);
            }
        });


        // NE MARCHE PAS PROBLEME DE CONNEXION
        /*final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.21/appspace_api/index.php/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        AppSpaceApi appSpaceApi = retrofit.create(AppSpaceApi.class);
        Call<List<Espace>> call = appSpaceApi.getEspaces();
        call.enqueue(new Callback<List<Espace>>() {
            @Override
            public void onResponse(Call<List<Espace>> call, Response<List<Espace>> response) {
                Log.d("Réponse", "ok");
                if(!response.isSuccessful()){
                    textViewTest.setText("Code:"+ response.code());
                    return;
                }

                List<Espace> espaces = response.body();

                for (Espace espace : espaces){
                    String content = "";
                    content += "id=" + espace.getId() + "\n";
                    textViewTest.setText(content);

                }
            }

            @Override
            public void onFailure(Call<List<Espace>> call, Throwable t) {
                Log.d("Réponse", t.getMessage(), t);

            }
        });*/

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // affichage des boutons du menu
        getMenuInflater().inflate(R.menu.drawer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent versPrefs = null;
        switch(id) {

            case R.id.logout :
                versPrefs = new Intent(this, MainActivity.class);
                startActivity(versPrefs);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
