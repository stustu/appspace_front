package com.example.appspace;

public class Indicateur {
    private int id;
    private String nom;
    private Enum type;
    private int valeurInt;
    private String valeurString;
    private Boolean valeurBoolean;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Enum getType() {
        return type;
    }

    public void setType(Enum type) {
        this.type = type;
    }

    public int getValeurInt() {
        return valeurInt;
    }

    public void setValeurInt(int valeurInt) {
        this.valeurInt = valeurInt;
    }

    public String getValeurString() {
        return valeurString;
    }

    public void setValeurString(String valeurString) {
        this.valeurString = valeurString;
    }

    public Boolean getValeurBoolean() {
        return valeurBoolean;
    }

    public void setValeurBoolean(Boolean valeurBoolean) {
        this.valeurBoolean = valeurBoolean;
    }
}
